# File: /tests.sh
# Project: kwyzod
# File Created: 24-09-2023 12:27:12
# Author: Clay Risser
# -----
# BitSpur (c) Copyright 2023
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# boolean
for d in $(sh ./kwyzod.sh dialogs) fallback; do
    echo 'kwyzod --dialogs '"$d"' boolean "Do you like kwyzod?"'
    sh ./kwyzod.sh --dialogs $d boolean "Do you like kwyzod?"
done

# string
for d in $(sh ./kwyzod.sh dialogs) fallback; do
    echo 'kwyzod --dialogs '"$d"' string -d "Bob" "What is your name?'
    sh ./kwyzod.sh --dialogs $d string -d "Bob" "What is your name?"
done

# integer
for d in $(sh ./kwyzod.sh dialogs) fallback; do
    echo 'kwyzod --dialogs '"$d"' integer -d "88" "What is your favorite number?'
    sh ./kwyzod.sh --dialogs $d integer -d "88" "What is your favorite number?"
done

# enum
for d in $(sh ./kwyzod.sh dialogs) fallback; do
    echo 'kwyzod --dialogs '"$d"' enum -d "blue" "What is your favorite color?" red orange yellow green indigo violet "none of the above"'
    sh ./kwyzod.sh --dialogs $d enum -d "blue" "What is your favorite color?" red orange yellow green indigo violet "none of the above"
done
for d in $(sh ./kwyzod.sh dialogs) fallback; do
    echo 'kwyzod --dialogs '"$d"' enum -m -d "car" "What are your favorite animals?" fish cat dog monkey zebra  "none of the above"'
    sh ./kwyzod.sh --dialogs $d enum -m -d "car" "What are your favorite animals?" fish cat dog monkey zebra "none of the above"
done
