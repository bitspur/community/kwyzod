# kwyzod

> universal dialogs for shell scripts

**kwyzod** is a robust shell script that provides a unified interface for a variety of dialog types. It supports user input in the form of boolean, string, integer, enum, and multiselect options. With a design focused on compatibility, kwyzod operates seamlessly on OSX, Linux, and is expected to function in any Unix environment. As a POSIX compliant script, it ensures broad usability and adaptability across diverse systems.

The name **kwyzod** is an acronym representing the dialogs it supports:

**K**dialog - A dialog interface for KDE

**W**hiptail - A dialog interface for terminal-based software

**Y**ad - Yet Another Dialog, a tool for creating graphical dialogs from shell scripts

**Z**enity - A tool for displaying GTK+ dialogs in command-line scripts

**O**sascript - A scripting language for Mac OS X, used to display dialogs

**D**ialog - A tool for creating professional-looking dialogs within shell scripts
